from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect, HttpRequest, HttpResponse
from medical_module.LimeSurveyRemote.LimeSurveyRemote import LimeSurveyRemote
import logging

logger = logging.getLogger(__name__)


# Create your views here.
def index(request):
    return HttpResponseRedirect('https://patient-profile-gito.limequery.com/259777?newtest=Y&lang=ru')


def main_page(request):
    return render(request, 'medical_module/mainPage.html')


def list_patient(request):
    search_query = request.GET.get('search', '')
    filter_option = [request.GET.get('mainPathologySelect', ''), request.GET.get('genderSelect', ''),
                     request.GET.get('levelSelect', ''),
                     request.GET.get('disbalanceSelect', ''), request.GET.get('dateSelect', ''),
                     request.GET.get('dateOrder', ''), request.GET.get('orgSelect', '')]
    logger.debug("Firing signal with: %s", filter_option)
    try:
        remote_access = LimeSurveyRemote()
        list_responses = remote_access.filters(filter_option)
        if search_query:
            list_responses = remote_access.find_patient_id(search_query)
        context = {'responses': list_responses}

    except NameError:
        context = {}

    return render(request, 'medical_module/listPatients.html', context=context)


def show(request, id):
    remote_access = LimeSurveyRemote()
    remote_access.get_exported_dict_data()
    context = {'question': remote_access.get_list_of_patient_data(id)}
    return render(request, 'medical_module/patientInfo.html', remote_access.get_list_of_patient_data(id))


def doctor_menu(request):
    return render(request, 'medical_module/doctorMenu.html')