from django.apps import AppConfig


class MedicalModuleConfig(AppConfig):
    name = 'medical_module'
