from django.conf.urls import url
from . import views
from django.urls import path
from django.conf.urls import url

urlpatterns = [
    path('start_survey/', views.index, name='start_survey'),
    path('', views.main_page),
    path('mainMenu/', views.doctor_menu, name='doctor_menu'),
    path('list_patients/', views.list_patient, name='list_patient'),
    url(r'^show/(?P<id>\d+)$', views.show, name='show'),
]
