import pandas as pd  # For data frames in a format PowerBI understands
from limesurveyrc2api.limesurvey import LimeSurvey  # To talk to LimeSurvey API
from limesurveyrc2api.exceptions import LimeSurveyError
import io
import base64  # To convert base64 encoded data
import json
import ast
import logging

logger = logging.getLogger(__name__)


class LimeSurveyRemote:
    remotecontrolurl = ''
    limeuser = ''
    limepasswd = ''
    surveyid = ''
    list_clear = None

    def __init__(self):
        self.lime = LimeSurvey(url=self.remotecontrolurl, username=self.limeuser)
        self.lime.open(password=self.limepasswd)

    def get_exported_dict_data(self):
        result = self.lime.survey.export_responses(survey_id=self.surveyid, document_type='json', heading_type='full',
                                                   response_type='long')
        survey_data = pd.read_json(io.StringIO(base64.b64decode(result).decode("utf-8")), orient='index')
        df = pd.DataFrame(survey_data)
        with open('exported_data.json', 'w', encoding='utf-8') as file:
            df.to_json(file, force_ascii=False)

        with open('exported_data.json', encoding='utf-8') as fh:  # открываем файл на чтение
            data = json.load(fh)
        data = ast.literal_eval(str(data).replace('\\xa0', ' '))
        list_full_data = list(data.values())
        self.list_clear = []
        for i in range(len(list_full_data)):
            list_values = list(list_full_data[i].values())
            for j in range(len(list_values)):
                self.list_clear.append(list(list_values[j].values())[0])
        for i in range(len(self.list_clear)):
            self.list_clear[i]['id'] = self.list_clear[i].get('Идентификатор пациента ')
            self.list_clear[i]['Дата выписки '] = self.list_clear[i].get('Дата выписки ').split(' ')[0]
            self.list_clear[i]['Дата операции '] = self.list_clear[i].get('Дата операции ').split(' ')[0]
            self.list_clear[i]['Дата рождения'] = self.list_clear[i].get('Дата рождения').split(' ')[0]
            self.list_clear[i]['mainPathology'] = self.list_clear[i].get('Основная патология ').strip()
            self.list_clear[i]['Дата поступления '] = self.list_clear[i].get('Дата поступления ').split(' ')[0]
            temp_str = self.list_clear[i]['Дата поступления ']
            temp_str = temp_str.split('-')
            temp_str = temp_str[2] + '.' + temp_str[1] + '.' + temp_str[0]
            self.list_clear[i]['receiptDate'] = temp_str
            temp_dic = self.list_clear[i]
            keys = list(temp_dic.keys())
            for key in keys:
                value = temp_dic.get(key)
                self.list_clear[i][key] = str(value).strip()
                if key.find('['):
                    if value == 'Нет':
                        self.list_clear[i].pop(key, None)
                if not value or value == 'N/A' or value == '':
                    self.list_clear[i].pop(key, None)
        return self.list_clear

    def get_list_of_patient_data(self, patient_id):
        data_list = []
        main_list = []
        level_list = []
        pathology_list = []
        surgery_list = []
        surgical_measures_list = []
        hospitalization_list = []
        for i in range(len(self.list_clear)):
            temp_dic = self.list_clear[i]
            if temp_dic.get('Идентификатор пациента ') == str(patient_id):
                value = []
                keys = list(temp_dic.keys())[list(temp_dic.keys()).index("Наименование учреждения "):len(temp_dic) - 3]
                for key in keys:
                    if key.find('[') and temp_dic.get(key) == 'Да':
                        value.append('')
                    else:
                        value.append(temp_dic.get(key))
                for j in range(len(keys)):
                    if not value[j]:
                        temp_list = [keys[j]]
                    else:
                        temp_list = [keys[j] + ': ' + value[j]]
                    data_list.append(temp_list)
                    if 'Идентификатор пациента' in temp_list[0] or j < 4:
                        main_list.append(temp_list)
                    if 'Уровень проведения процедуры' in temp_list[0]:
                        level_list.append(temp_list)
                    if 'Дата пос' in temp_list[0] or (
                            pathology_list and 'Хирург' not in temp_list[0] and not surgery_list):
                        pathology_list.append(temp_list)
                    if 'Хирург' in temp_list[0] or (
                            surgery_list and 'Объем де' not in temp_list[0] and not surgical_measures_list):
                        surgery_list.append(temp_list)
                    if 'Объем декомпрессии' in temp_list[0] or (
                            surgical_measures_list and 'Ранние послеоперационные' not in temp_list[
                        0] and not hospitalization_list):
                        surgical_measures_list.append(temp_list)
                    if 'Ранние послеоперационные осложнения' in temp_list[0] or hospitalization_list:
                        hospitalization_list.append(temp_list)
                    data_list.append(temp_list)
        return {'main': main_list, 'level': level_list, 'pathology': pathology_list,
                'surgery': surgery_list, 'measure': surgical_measures_list, 'hospital': hospitalization_list}

    def find_patient_id(self, patient_id):
        for i in range(len(self.list_clear)):
            temp_dic = self.list_clear[i]
            if temp_dic.get('Идентификатор пациента ') == str(patient_id):
                return [temp_dic]

    def filters(self, list_filters):
        filtered_list = self.get_exported_dict_data()
        if list_filters[6]:
            filtered_list = self.organization_name(list_filters[6], filtered_list)
        if list_filters[0]:
            filtered_list = self.main_pathology_filter(list_filters[0], filtered_list)
        if list_filters[1]:
            filtered_list = self.gender_filter(list_filters[1], filtered_list)
        if list_filters[2]:
            filtered_list = self.level_filter(list_filters[2], filtered_list)
        if list_filters[3]:
            filtered_list = self.disbalance_filter(list_filters[3], filtered_list)
        if list_filters[4] and list_filters[5]:
            filtered_list = self.date_filter(list_filters[4:6], filtered_list)
        return filtered_list

    def organization_name(self, filter_option, filtered_list):
        filtered_responses = []
        for i in range(len(filtered_list)):
            if filtered_list[i].get('Наименование учреждения ') == str(filter_option):
                filtered_responses.append(filtered_list[i])
        return filtered_responses

    def main_pathology_filter(self, filter_option, filtered_list):
        filtered_responses = []
        for i in range(len(filtered_list)):
            if filtered_list[i].get('Основная патология ') == str(filter_option):
                filtered_responses.append(filtered_list[i])
        return filtered_responses

    def gender_filter(self, filter_option, filtered_list):
        filtered_responses = []
        for i in range(len(filtered_list)):
            if filtered_list[i].get('Пол') == str(filter_option):
                filtered_responses.append(filtered_list[i])
        return filtered_responses

    def level_filter(self, filter_option, filtered_list):
        filtered_responses = []
        for i in range(len(filtered_list)):
            if filtered_list[i].get('Уровень проведения процедуры ') == str(filter_option):
                filtered_responses.append(filtered_list[i])
        return filtered_responses

    def disbalance_filter(self, filter_option, filtered_list):
        filtered_responses = []
        for i in range(len(filtered_list)):
            if filtered_list[i].get('Дисбаланс') == str(filter_option):
                filtered_responses.append(filtered_list[i])
        return filtered_responses

    def date_filter(self, filter_option, filtered_list):
        if filter_option[0] == '1':
            if filter_option[1] == '1':
                return sorted(filtered_list, key=lambda k: k['Дата рождения'])
            else:
                return sorted(filtered_list, key=lambda k: k['Дата рождения'], reverse=True)
        if filter_option[0] == '2':
            if filter_option[1] == '1':
                return sorted(filtered_list, key=lambda k: k['Дата поступления '])
            else:
                return sorted(filtered_list, key=lambda k: k['Дата поступления '], reverse=True)
        if filter_option[0] == '3':
            if filter_option[1] == '1':
                return sorted(filtered_list, key=lambda k: k['Дата операции '])
            else:
                return sorted(filtered_list, key=lambda k: k['Дата операции '], reverse=True)
        if filter_option[0] == '4':
            if filter_option[1] == '1':
                return sorted(filtered_list, key=lambda k: k['Дата выписки '])
            else:
                return sorted(filtered_list, key=lambda k: k['Дата выписки '], reverse=True)